<GameFile>
  <PropertyGroup Name="GameOverStats" Type="Layer" ID="6551e3c0-476b-4537-ab9b-223f049c78ea" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" CustomClassName="GameOverClass" Tag="291" ctype="GameLayerObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="background_1" ActionTag="-686360404" Tag="292" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="-213.3696" RightMargin="1109.3696" TopMargin="217.7594" BottomMargin="222.2406" ctype="SpriteObjectData">
            <Size X="1024.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="298.6304" Y="542.2406" />
            <Scale ScaleX="0.6413" ScaleY="1.6948" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1555" Y="0.5021" />
            <PreSize X="0.5333" Y="0.5926" />
            <FileData Type="Normal" Path="background.jpg" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Panel_Stats" ActionTag="538593983" Alpha="153" Tag="293" IconVisible="False" LeftMargin="50.5899" RightMargin="1669.4100" TopMargin="238.6151" BottomMargin="641.3849" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="labelCoin" ActionTag="1931708689" Tag="294" IconVisible="False" LeftMargin="22.6076" RightMargin="97.3924" TopMargin="63.1853" BottomMargin="113.8147" FontSize="20" LabelText="labelCoin	" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="80.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="62.6076" Y="125.3147" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3130" Y="0.6266" />
                <PreSize X="0.4000" Y="0.1150" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="labelDistance" ActionTag="227652051" Tag="295" IconVisible="False" LeftMargin="22.6075" RightMargin="60.3925" TopMargin="96.6542" BottomMargin="80.3458" FontSize="20" LabelText="labelDistance" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="117.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="81.1075" Y="91.8458" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4055" Y="0.4592" />
                <PreSize X="0.5850" Y="0.1150" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="labelScore" ActionTag="354466068" Tag="296" IconVisible="False" LeftMargin="22.6075" RightMargin="84.3925" TopMargin="130.1232" BottomMargin="46.8768" FontSize="20" LabelText="labelScore" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="93.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5191" ScaleY="0.2699" />
                <Position X="70.8806" Y="53.0842" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3544" Y="0.2654" />
                <PreSize X="0.4650" Y="0.1150" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="labelKill" ActionTag="1284661318" Tag="297" IconVisible="False" LeftMargin="22.6075" RightMargin="111.3925" TopMargin="163.5921" BottomMargin="-9.5921" FontSize="20" LabelText="labelKill&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="66.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.6075" Y="13.4079" />
                <Scale ScaleX="0.9323" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2780" Y="0.0670" />
                <PreSize X="0.3300" Y="0.2300" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="50.5899" Y="641.3849" />
            <Scale ScaleX="2.7500" ScaleY="2.7500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0263" Y="0.5939" />
            <PreSize X="0.1042" Y="0.1852" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Panel_btn" ActionTag="1870371148" Tag="297" IconVisible="False" LeftMargin="62.8808" RightMargin="1657.1193" TopMargin="837.3768" BottomMargin="42.6232" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="btn_Resume" ActionTag="47531726" Tag="298" IconVisible="False" LeftMargin="-12.6863" RightMargin="82.6863" TopMargin="-32.3464" BottomMargin="102.3464" TouchEnable="True" FontSize="24" ButtonText="Resume" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="93" Scale9Height="101" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="130.0000" Y="130.0000" />
                <AnchorPoint ScaleX="0.4794" ScaleY="0.3876" />
                <Position X="49.6305" Y="152.7357" />
                <Scale ScaleX="0.4505" ScaleY="0.4893" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2482" Y="0.7637" />
                <PreSize X="0.6500" Y="0.6500" />
                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                <TextColor A="255" R="0" G="0" B="0" />
                <NormalFileData Type="Normal" Path="btn/button_artbord-01.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_restart" ActionTag="336246614" Tag="299" IconVisible="False" LeftMargin="-12.0038" RightMargin="82.0038" TopMargin="42.4856" BottomMargin="27.5144" TouchEnable="True" FontSize="24" ButtonText="Restart" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="93" Scale9Height="101" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="130.0000" Y="130.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="52.9962" Y="92.5144" />
                <Scale ScaleX="0.4401" ScaleY="0.4537" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2650" Y="0.4626" />
                <PreSize X="0.6500" Y="0.6500" />
                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                <TextColor A="255" R="0" G="0" B="0" />
                <NormalFileData Type="Normal" Path="btn/button_artbord-03.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_home" ActionTag="-695951229" Tag="300" IconVisible="False" LeftMargin="-12.5939" RightMargin="82.5939" TopMargin="110.6847" BottomMargin="-40.6847" TouchEnable="True" FontSize="24" ButtonText="Home" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="101" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="130.0000" Y="130.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="52.4061" Y="24.3153" />
                <Scale ScaleX="0.4609" ScaleY="0.4609" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2620" Y="0.1216" />
                <PreSize X="0.6500" Y="0.6500" />
                <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                <TextColor A="255" R="26" G="26" B="26" />
                <NormalFileData Type="Normal" Path="btn/button_artbord-11.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="62.8808" Y="42.6232" />
            <Scale ScaleX="3.0587" ScaleY="2.9065" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0328" Y="0.0395" />
            <PreSize X="0.1042" Y="0.1852" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>