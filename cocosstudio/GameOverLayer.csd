<GameFile>
  <PropertyGroup Name="GameOverLayer" Type="Layer" ID="ff5cf25d-9e86-4f00-82be-148a99fc7c00" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000">
        <Timeline ActionTag="2067545748" Property="ActionValue">
          <InnerActionFrame FrameIndex="0" Tween="False" InnerActionType="SingleFrame" CurrentAniamtionName="-- ALL --" SingleFrameIndex="0" />
        </Timeline>
        <Timeline ActionTag="2067545748" Property="Position">
          <PointFrame FrameIndex="0" X="579.1599" Y="3.5722">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="2067545748" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.6743" Y="0.9922">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="2067545748" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="93" ctype="GameLayerObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="FileNode_1" ActionTag="2067545748" Tag="127" IconVisible="True" LeftMargin="579.1599" RightMargin="-579.1599" TopMargin="-3.5721" BottomMargin="3.5722" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position X="579.1599" Y="3.5722" />
            <Scale ScaleX="0.6743" ScaleY="0.9922" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3016" Y="0.0033" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="GameOverView.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_2" ActionTag="-887736210" Tag="301" IconVisible="True" LeftMargin="-3.0723" RightMargin="3.0723" TopMargin="-0.0005" BottomMargin="0.0005" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position X="-3.0723" Y="0.0005" />
            <Scale ScaleX="1.1872" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.0016" Y="0.0000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="GameOverStats.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>