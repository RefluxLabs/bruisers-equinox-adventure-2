<GameFile>
  <PropertyGroup Name="StoreLayer_0" Type="Layer" ID="b409c6d3-956c-472d-99e6-c29e102c3035" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" UserData="StoreLayer Panel" Tag="39" ctype="GameLayerObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="-1318797230" Tag="44" IconVisible="False" LeftMargin="-0.0168" RightMargin="1720.0168" TopMargin="880.0074" BottomMargin="-0.0074" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="2" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint />
            <Position X="-0.0168" Y="-0.0074" />
            <Scale ScaleX="9.5999" ScaleY="5.4000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0000" Y="0.0000" />
            <PreSize X="0.1042" Y="0.1852" />
            <SingleColor A="255" R="117" G="117" B="214" />
            <FirstColor A="255" R="255" G="255" B="0" />
            <EndColor A="255" R="255" G="165" B="0" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ListView_1" ActionTag="-66557744" Tag="48" IconVisible="False" LeftMargin="210.6600" RightMargin="1509.3401" TopMargin="659.5813" BottomMargin="220.4187" TouchEnable="True" ClipAble="True" BackColorAlpha="81" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" ItemMargin="25" DirectionType="Vertical" HorizontalType="Align_HorizontalCenter" ctype="ListViewObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Item_1" ActionTag="-580150965" Tag="47" IconVisible="False" BottomMargin="200.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="177" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="75.0000" />
                <Children>
                  <AbstractNodeData Name="Button_9" ActionTag="-1698390326" Tag="108" IconVisible="False" LeftMargin="99.9490" RightMargin="-25.9490" TopMargin="-18.2808" BottomMargin="-29.7192" TouchEnable="True" FontSize="72" ButtonText="50" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="101" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="126.0000" Y="123.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="162.9490" Y="31.7808" />
                    <Scale ScaleX="0.2606" ScaleY="0.4602" />
                    <CColor A="255" R="127" G="127" B="127" />
                    <PrePosition X="0.8147" Y="0.4237" />
                    <PreSize X="0.6300" Y="1.6400" />
                    <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                    <TextColor A="255" R="0" G="0" B="0" />
                    <NormalFileData Type="Normal" Path="btn/button_artbord-11.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Sprite_3" ActionTag="-64640751" Tag="109" IconVisible="False" LeftMargin="-235.7848" RightMargin="-72.2152" TopMargin="-215.9256" BottomMargin="-217.0744" ctype="SpriteObjectData">
                    <Size X="508.0000" Y="508.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="18.2152" Y="36.9256" />
                    <Scale ScaleX="0.0635" ScaleY="0.1238" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0911" Y="0.4923" />
                    <PreSize X="2.5400" Y="6.7733" />
                    <FileData Type="Normal" Path="btn/Prop_Flask1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_12" ActionTag="46763392" Tag="110" IconVisible="False" LeftMargin="-192.3758" RightMargin="-164.6242" TopMargin="17.7003" BottomMargin="-2.7003" FontSize="24" LabelText="Prop_flask1 drink this to obtain magnetic properties&#xA;" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="557.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="86.1242" Y="27.2997" />
                    <Scale ScaleX="0.1768" ScaleY="1.0894" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4306" Y="0.3640" />
                    <PreSize X="2.7850" Y="0.8000" />
                    <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="200.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.7273" />
                <PreSize X="1.0000" Y="0.2727" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Item_2" ActionTag="-1823845011" ZOrder="1" Tag="111" IconVisible="False" TopMargin="100.0000" BottomMargin="100.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="209" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="75.0000" />
                <Children>
                  <AbstractNodeData Name="Button_9" ActionTag="1096067840" Tag="112" IconVisible="False" LeftMargin="103.0277" RightMargin="-29.0277" TopMargin="-11.7683" BottomMargin="-36.2317" TouchEnable="True" FontSize="72" ButtonText="50" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="101" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="126.0000" Y="123.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="166.0277" Y="25.2683" />
                    <Scale ScaleX="0.2099" ScaleY="0.3707" />
                    <CColor A="255" R="127" G="127" B="127" />
                    <PrePosition X="0.8301" Y="0.3369" />
                    <PreSize X="0.6300" Y="1.6400" />
                    <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                    <TextColor A="255" R="0" G="0" B="0" />
                    <NormalFileData Type="Normal" Path="btn/button_artbord-11.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Sprite_3" ActionTag="2100117869" Tag="113" IconVisible="False" LeftMargin="-235.0461" RightMargin="-72.9539" TopMargin="-215.7840" BottomMargin="-217.2160" ctype="SpriteObjectData">
                    <Size X="508.0000" Y="508.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="18.9539" Y="36.7840" />
                    <Scale ScaleX="0.0743" ScaleY="0.1448" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0948" Y="0.4905" />
                    <PreSize X="2.5400" Y="6.7733" />
                    <FileData Type="Normal" Path="btn/Prop_Flask1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_12" ActionTag="-497522761" Tag="114" IconVisible="False" LeftMargin="-179.8891" RightMargin="-177.1109" TopMargin="8.2160" BottomMargin="6.7840" FontSize="24" LabelText="Prop_flask1 drink this to obtain magnetic properties&#xA;" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="557.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="98.6109" Y="36.7840" />
                    <Scale ScaleX="0.1941" ScaleY="0.9433" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4931" Y="0.4905" />
                    <PreSize X="2.7850" Y="0.8000" />
                    <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.3636" />
                <PreSize X="1.0000" Y="0.2727" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Item_3" ActionTag="796658503" ZOrder="2" Tag="115" IconVisible="False" TopMargin="200.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="75.0000" />
                <Children>
                  <AbstractNodeData Name="Button_9" ActionTag="-750900880" Tag="116" IconVisible="False" LeftMargin="117.0113" RightMargin="-43.0113" TopMargin="-23.2840" BottomMargin="-24.7160" TouchEnable="True" FontSize="72" ButtonText="50" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="101" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="126.0000" Y="123.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="180.0113" Y="36.7840" />
                    <Scale ScaleX="0.3274" ScaleY="0.5782" />
                    <CColor A="255" R="127" G="127" B="127" />
                    <PrePosition X="0.9001" Y="0.4905" />
                    <PreSize X="0.6300" Y="1.6400" />
                    <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                    <TextColor A="255" R="0" G="0" B="0" />
                    <NormalFileData Type="Normal" Path="btn/button_artbord-11.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Sprite_3" ActionTag="-1779495827" Tag="117" IconVisible="False" LeftMargin="-235.0461" RightMargin="-72.9539" TopMargin="-215.7840" BottomMargin="-217.2160" ctype="SpriteObjectData">
                    <Size X="508.0000" Y="508.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="18.9539" Y="36.7840" />
                    <Scale ScaleX="0.0743" ScaleY="0.1448" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0948" Y="0.4905" />
                    <PreSize X="2.5400" Y="6.7733" />
                    <FileData Type="Normal" Path="btn/Prop_Flask1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_12" ActionTag="-1092142961" Tag="118" IconVisible="False" LeftMargin="-179.8893" RightMargin="-177.1107" TopMargin="3.7772" BottomMargin="11.2228" FontSize="24" LabelText="Prop_flask1 drink this to obtain magnetic properties&#xA;" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="557.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="98.6107" Y="41.2228" />
                    <Scale ScaleX="0.1941" ScaleY="0.9433" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4931" Y="0.5496" />
                    <PreSize X="2.7850" Y="0.8000" />
                    <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="0.2727" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="210.6600" Y="220.4187" />
            <Scale ScaleX="7.8611" ScaleY="3.3692" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1097" Y="0.2041" />
            <PreSize X="0.1042" Y="0.1852" />
            <SingleColor A="255" R="127" G="127" B="127" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Panel_Stats" ActionTag="-299830683" ZOrder="1" Tag="104" IconVisible="False" LeftMargin="209.2075" RightMargin="1510.7925" TopMargin="867.8656" BottomMargin="12.1344" TouchEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="txt_coins" ActionTag="1255587959" CallBackType="Touch" UserData="txt_coinsEvent" Tag="111" IconVisible="False" LeftMargin="144.3554" RightMargin="-5.3554" TopMargin="114.2100" BottomMargin="-49.2100" FontSize="24" LabelText="Coins&#xA;&#xA;&#xA;&#xA;" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="61.0000" Y="135.0000" />
                <AnchorPoint ScaleX="0.6032" ScaleY="0.3310" />
                <Position X="181.1506" Y="-4.5250" />
                <Scale ScaleX="0.6792" ScaleY="1.9849" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.9058" Y="-0.0226" />
                <PreSize X="0.3050" Y="0.6750" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_magnet" ActionTag="1625825666" CallBackType="Touch" UserData="txt_magnet" Tag="70" IconVisible="False" LeftMargin="17.6609" RightMargin="174.3391" TopMargin="179.8120" BottomMargin="-11.8120" FontSize="14" LabelText="0&#xA;" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="8.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.4938" ScaleY="0.1511" />
                <Position X="21.6113" Y="-6.9768" />
                <Scale ScaleX="2.4655" ScaleY="4.2757" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.1081" Y="-0.0349" />
                <PreSize X="0.0400" Y="0.1600" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_shoes" ActionTag="1412499347" CallBackType="Touch" UserData="txt_purpleshoes" Tag="73" FrameEvent="btn_buyshoesEvent" IconVisible="False" LeftMargin="72.9063" RightMargin="119.0937" TopMargin="158.5822" BottomMargin="9.4178" FontSize="14" LabelText="0&#xA;" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="8.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5904" ScaleY="0.3330" />
                <Position X="77.6295" Y="20.0738" />
                <Scale ScaleX="2.3069" ScaleY="3.8710" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.3881" Y="0.1004" />
                <PreSize X="0.0400" Y="0.1600" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_redshoes" ActionTag="1907287392" CallBackType="Touch" UserData="txt_redshoes" Tag="74" IconVisible="False" LeftMargin="124.8418" RightMargin="67.1582" TopMargin="151.7270" BottomMargin="16.2730" FontSize="14" LabelText="0&#xA;" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="8.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5060" ScaleY="0.3968" />
                <Position X="128.8898" Y="28.9706" />
                <Scale ScaleX="2.1154" ScaleY="3.5701" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.6444" Y="0.1449" />
                <PreSize X="0.0400" Y="0.1600" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="209.2075" Y="12.1344" />
            <Scale ScaleX="7.8671" ScaleY="1.0538" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1090" Y="0.0112" />
            <PreSize X="0.1042" Y="0.1852" />
            <SingleColor A="255" R="229" G="229" B="229" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_back" ActionTag="2138858367" UserData="btn_back" Tag="50" IconVisible="False" LeftMargin="65.4976" RightMargin="1731.5024" TopMargin="947.8807" BottomMargin="9.1193" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="93" Scale9Height="101" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="123.0000" Y="123.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="126.9976" Y="70.6193" />
            <Scale ScaleX="1.1887" ScaleY="0.9277" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0661" Y="0.0654" />
            <PreSize X="0.0641" Y="0.1139" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="btn/button_artbord-03.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Title" ActionTag="-1122555919" Tag="43" IconVisible="False" LeftMargin="848.1841" RightMargin="1003.8159" TopMargin="58.9449" BottomMargin="996.0551" FontSize="20" LabelText="Market " HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="68.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.4336" ScaleY="0.5283" />
            <Position X="877.6689" Y="1009.2626" />
            <Scale ScaleX="14.3903" ScaleY="7.7898" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.4571" Y="0.9345" />
            <PreSize X="0.0354" Y="0.0231" />
            <FontResource Type="Normal" Path="font/Milonga-Regular.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>