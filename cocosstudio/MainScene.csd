<GameFile>
  <PropertyGroup Name="MainScene" Type="Scene" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Default" ActionTag="953446860" Tag="5" IconVisible="False" LeftMargin="479.7964" RightMargin="480.2036" TopMargin="230.0941" BottomMargin="209.9059" ctype="SpriteObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="959.7964" Y="529.9059" />
            <Scale ScaleX="2.0000" ScaleY="1.7187" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4999" Y="0.4907" />
            <PreSize X="0.5000" Y="0.5926" />
            <FileData Type="Normal" Path="HelloWorld.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_1" ActionTag="-259248949" Tag="65" IconVisible="True" LeftMargin="-0.0007" RightMargin="0.0007" TopMargin="0.0017" BottomMargin="-0.0017" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position X="-0.0007" Y="-0.0017" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0000" Y="0.0000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="StoreLayer_0.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>