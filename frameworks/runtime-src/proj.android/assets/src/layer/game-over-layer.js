///////////////////////////////////////////////
//Game Over Layer = Statistics Layer in
//statistics-layer.js and alternative would be to go to
//bitbucket before gameoverlayer commit and look at the file
//game-over-layer.js <- this file holds data without the upload functionality
///////////////////////////////////////////////

var StatisticsLayer = cc.LayerColor.extend(/**@lends StatisticsLayer# */{
	labelCoin: null,
	statistics: null,
    board:null,
	_className:"GameOverClass",
	cval : 0,
	sval : 0,
	kval : 0,
	mval : 0,

	openStore: false,
	// constructor
	ctor:function (statistics, playSceneClass) {
		this._super();
		this.init(cc.color(0, 0, 0, 80));
		if (! playSceneClass) {
			playSceneClass = PlayScene;
		}
		this.playSceneClass = playSceneClass;
		this.statistics = statistics;
		firstInit = false;

        ////////////////////////////////////////
		//** Load Game Over Json! ***
		////////////////////////////////////////
        var gameoverlayer = ccs.load(res.IAP.GameOverLayer_json);
        this.addChild(gameoverlayer.node);
		cc.log("loading GameOverLayer.json!");
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
			//cc.audioEngine.playMusic(res.sound.shopping, true);
			////////////////////////////////////////////
			//*** Declare FileNode_1 && FileNode_2
			///////////////////////////////////////////
			//@Param:FileNode_1:tag
			//@Param:FileNode_2:301
			//|||||||||||||||||||||||||||||||||||||
			var GameStats = gameoverlayer.node.getChildByTag(301);
			cc.log(GameStats);
			cc.log("GameStats above!" + GameStats);
			//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

				////////////////////////////////////////////
				//*** Declare Panel_Labels && Panel_btn
				///////////////////////////////////////////
				//**Access Point: GameStats.Panel_
				//@Param:Panel_Stats:293
				//@Param:Panel_btn:297
				var Panel_Stats = GameStats.getChildByTag(293);
				cc.log("Panel_Stats" + Panel_Stats);
				//|||||||||||||||||||||||||||||||||||
				var Panel_btn = GameStats.getChildByTag(297);
				cc.log("Panel_btn" + Panel_btn);
				//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

					///////////////////////////////////////
					//** Define Panel_Label Objects***
					//////////////////////////////////////
					//@Param: this.labelCoin:294
					//@Param: this.labelDistance:295
					//@Param: this.labelScore:295

					this.labelCoin = Panel_Stats.getChildByTag(294);
					cc.log("this.labelCoin" + this.labelCoin);

					this.labelDistance = Panel_Stats.getChildByTag(295);
					cc.log("this.labelCoin" + this.labelDistance);

					this.labelScore = Panel_Stats.getChildByTag(296);
					cc.log("this.labelScore" + this.labelScore);

					this.labelKill = Panel_Stats.getChildByTag(297);
					cc.log("this.labelKill" + this.labelKill);
					//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

					///////////////////////////////////////
					//** Define Panel_btn Objects
					//@Param: btn_Resume:298
					//@Param: btn_restart:299
					//@Param: btn_home:300
					//|||||||||||||||||||||||||||||||
					var btn_resume = Panel_btn.getChildByTag(298);
					cc.log("btn_Resume" + btn_resume);
					var btn_restart = Panel_btn.getChildByTag(299);
					cc.log("btn_restart" + btn_restart);
					var btn_home = Panel_btn.getChildByTag(300);
					cc.log("btn_home" + btn_home);
					//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

				//////////////////////////////////////////////////
				//*** Declaration of Touch Event For buttons ***//
				//////////////////////////////////////////////////
	            btn_resume.addTouchEventListener(this.btn_resumeEvent, this);
	            btn_restart.addTouchEventListener(this.btn_restartEvent, this);
	            btn_home.addTouchEventListener(this.btn_homeEvent, this);
				//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		////////////////////////////////////////////
		//*** todo declare Labels ***
		/////////////////////////////////////////////
		this.score = statistics.kill * 30 + statistics.coins*10 + statistics.meter;

        ///////////////////////////////////////////////
		//*** save data to localstorage ****
		////////////////////////////////////////////
		var coinsNum = sys.localStorage.getItem("TotalCoin");
		if(!isNaN(coinsNum)) {
			newCoinsNum = parseInt(coinsNum) + parseInt(statistics.coins);
			sys.localStorage.setItem("TotalCoin",newCoinsNum);
			var a = sys.localStorage.getItem("TotalCoin");
		} else {
			sys.localStorage.setItem("TotalCoin",0);
			coinsNum = sys.localStorage.getItem("TotalCoin");
			newCoinsNum = parseInt(statistics.coins);
			sys.localStorage.setItem("TotalCoin",newCoinsNum);
			var a = sys.localStorage.getItem("TotalCoin");
		}

	////////////////////////////////////////////////////////////
	//**todo PageViewLayer.json object **//
	///////////////////////////////////////////////////////////
	//PageViewLayer{Lends from @GameOverLayer.json//
	//@Param Access Point:FileNode_1:127 //ObjectData : Tag 93
//		PageViewLayer = gameoverlayer.node.getChildByTag(127);
//		cc.log("Loading GameOverViewLayer below!");
//		cc.log(PageViewLayer);
//            /////////////////////////////////////////////
//            //Declare Scope of PageView_1 to access buttons
//            //@param:PageView_1:
//            var PageView_1 = PageViewLayer.getChildByTag(107);
//            cc.log(PageView_1);
//
//                    ///////////////////////////////////////
//                    //Declaration of Panels within PageView_1
//                    //@Param:Panel_2:109
//                    //@Param:Panel_3:124
//                    //@Param:Panel_15:157
//                    var Panel_1 = PageView_1.getChildByTag(109);
//                    cc.log(Panel_1);
//                    ////////////////////////////////////
//                    //Within Scope PageView_1 Declare Panels
//                    //use tag bc name rending this deep is
//                    //wrong on web it says the name
//                    //for btn_highscore = button_1 ?
//                    //@Param:btn_highscore:117
//                    //var btn_highscore = Panel_2.getChildByTag(117);
//                    //cc.log(btn_highscore);
//                    //todo create wormhole for panels within PageView!!
//                    ///////////////////////////////////////////////////////
//                    //**Panel_4 : 256  --Holds Panel_Sound
//                    //@Param: Panel_Sound:316 --contains sound objects
//                    var Panel_4 = PageView_1.getChildByTag(254);
//                    cc.log("Panel_4"  + Panel_4);
//                    //|||||||||||||||||||||||||||||||||||||||||||||||||||
//                        var Panel_Sound = Panel_4.getChildByTag(316);
//                        cc.log("Panel_Sound!"  + Panel_Sound);
//                        cc.log(Panel_Sound);
//                        //** Declare Slider_sound:320
//                        var Slider_sound = Panel_Sound.getChildByTag(320);
//                        cc.log(Slider_sound);
//                       //||||||||||||||||||||||||||||||||||||||||||||||||
//                       //** Add Listener to Slider**//
//                       Slider_sound.addEventListener(this.sliderEvent,this);

	},
	bar: function()
	{
		cc.log("within _> function bar! ");
		return;
	},

	update: function(dt) {
		var statistics = this.statistics;
		if(this.cval < statistics.coins){
			this.cval++;
		}

		if(this.kval < statistics.kill) {
			this.kval++;
		}

		if(this.mval < statistics.meter) {
			this.mval++;
		}

		if(this.sval < statistics.score) {
			if(this.score > 1500) {
				this.sval += 20;
			} else {
				this.sval += 10;
			}
		}

		this.labelCoin.setString("Coins :" + this.cval);
		this.labelKill.setString("Kills :" + this.kval);
		this.labelDistance.setString("Distance :" + this.mval);
		this.labelScore.setString("Score :" + this.sval);

		if(this.openStore) {
			this.labelCoins.setString(this.totalCoin);
			this.labelMagnet.setString(this.magnetNum);
			this.labelShoes.setString(this.shoesNum);
			this.labelRedshoes.setString(this.redshoesNum);
		}
	},
//        sliderEvent: function(sender,type)
//        {   switch(type)
//            {
//                case ccui.Slider.EVENT_PERCENT_CHANGED:
//                cc.log("percent" + sender.getPercent().toFixed(0));
//                var Volume = sender.getPercent();
//                Volume = Volume/100;
//                cc.audioEngine.setMusicVolume(Volume);
//                cc.log(Volume);
//                break;
//            }
//        },

	btn_restartEvent: function(sender, type)
    {
        switch (type)
        {
        case ccui.Widget.TOUCH_BEGAN:
            break;
        case ccui.Widget.TOUCH_MOVED:
            break;
        case ccui.Widget.TOUCH_ENDED:
            cc.log("btn_restartEvent  touch ended!")
            cc.director.runScene(new this.playSceneClass());
            break;
        case ccui.Widget.TOUCH_CANCELLED:
            break;
        }
    },

	btn_resumeEvent: function(sender, type)
    {
        switch (type)
        {
        case ccui.Widget.TOUCH_BEGAN:
            break;
        case ccui.Widget.TOUCH_MOVED:
            break;
        case ccui.Widget.TOUCH_ENDED:
            cc.log("btn_resumeEvent touch ended!")
//todo fix resume Event
//to fix this resume function is not within statistics layer or the old game-over-layer.j
//** to fix this problem you which was inherented from alto adventure game
//** You will need to research to create this task brand new object pausing
//algorthim will look like
//  if {Pause PlayScene()}
//		cc.director.runScene(GameOverLayer())<-- with Pause button enabled
// -- to make this eaiser to see draw out basic outline for this algorthim above!
//            var scene = new WelcomeScene();
//            cc.director.runScene(scene);
/////////////////////////////////////////////////////////////
//// example
//  // CallFunc without data
//  var finish = new cc.CallFunc(this.removeSprite, this);
//
//  // CallFunc with data
//  var finish = new cc.CallFunc(this.removeFromParentAndCleanup, this,  true);
            break;
        case ccui.Widget.TOUCH_CANCELLED:
            break;
        }
    },
	btn_homeEvent: function(sender, type)
    {
        switch (type)
        {
        case ccui.Widget.TOUCH_BEGAN:
            break;
        case ccui.Widget.TOUCH_MOVED:
            break;
        case ccui.Widget.TOUCH_ENDED:
            cc.log("btn_home  touch ended-->WelcomeScene()")
            //cc.director.purgeCachedData();
            cc.director.pushScene(new WelcomeScene());
            //cc.director.purgeDirector();
            break;
        case ccui.Widget.TOUCH_CANCELLED:
            break;
        }
    },

	onRestart: function (sender) {
		var winSize = cc.director.getWinSize();
//		var actionTo1 = cc.moveTo(0.7, cc.p(winSize.width/2-120, winSize.height/2-200)).easing(cc.easeBounceOut());
		var action = cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2)).easing(cc.easeElasticInOut(0.45)));
		this.board.runAction(action);
		this.restartBtn.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2-165)).easing(cc.easeElasticInOut(0.45)),
				cc.callFunc(function(){
				    ///////////////////////////////
				    //stop store music from play with playscene's game music!
				    cc.audioEngine.stopMusic();
				    ///////////////////////////
					cc.director.runScene(new this.playSceneClass());
				}.bind(this))));
		this.storeBtn.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2-165)).easing(cc.easeElasticInOut(0.45))));

		this.menuBtn.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2-165)).easing(cc.easeElasticInOut(0.45))));
		//play button effect
//		cc.audioEngine.playEffect(res.sound.button);

		//label out
		this.labelCoin.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2+25)).easing(cc.easeElasticInOut(0.45))));
		this.labelKill.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2+85)).easing(cc.easeElasticInOut(0.45))));
		this.labelDistance.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2-35)).easing(cc.easeElasticInOut(0.45))));
		this.labelScore.runAction(cc.sequence(
				cc.moveTo(0.7, cc.p(-300, winSize.height/2-95)).easing(cc.easeElasticInOut(0.45))));
	},

	onMenu: function() {
//		var action = cc.sequence(
//				cc.moveTo(0.7, cc.p(-300, winSize.height/2)).easing(cc.easeElasticInOut(0.45)));
//		this.board.runAction(action);
//		this.menuBtn.runAction(cc.sequence(
//				cc.moveTo(0.7, cc.p(-300, winSize.height/2-165)).easing(cc.easeElasticInOut(0.45)),
				cc.callFunc(function(){
					cc.director.replaceScene(new WelcomeScene());
				}.bind(this));
		//play button effect
	},

	onStore: function() {
		this.openStore = true;
		cc.audioEngine.stopMusic();
		this.totalCoin = sys.localStorage.getItem("TotalCoin");
		this.magnetNum = sys.localStorage.getItem("magnet");
		this.shoesNum = sys.localStorage.getItem("shoes");
		this.redshoesNum = sys.localStorage.getItem("redshoes");

		if(canMusicPlaying) {
//			cc.audioEngine.playMusic(res.sound.shopping, true);
		}
		var winsize = cc.director.getWinSize();
		this.draw = new cc.DrawNode();
		this.draw.drawRect(cc.p(0, winsize.height), cc.p(winsize.width, 0), cc.color(0, 0, 0, 80), 0, cc.color(0, 0, 0, 80));
		this.addChild(this.draw, 4, 1);

		cc.eventManager.addListener({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: true,
			onTouchBegan: function(){return true;},
		}, this.draw);

		this.sboard = new cc.Sprite(res.ui.storeBoard);
		this.sboard.setPosition(cc.p(winsize.width/2+300, winsize.height/2));
		this.sboard.setScale(0.57);
		this.addChild(this.sboard, 5);
		var actionTo = cc.moveTo(1, cc.p(winsize.width/2, winsize.height/2)).easing(cc.easeElasticOut());
		this.sboard.runAction(actionTo);

		this.backBtn = new cc.Menu(new cc.MenuItemSprite(
				new cc.Sprite(res.ui.backBtn),
				new cc.Sprite(res.ui.backBtn),
				this.backToMenu, this));
		this.backBtn.setPosition(cc.p(winsize.width+100, 60));
		this.backBtn.attr({
			anchorX: 0,
			anchorY: 0,
			x: winsize.width/2+300,
			y: winsize.height/2-190
		});
		this.backBtn.setScale(0.6);
		this.backBtn.runAction(cc.moveTo(1, cc.p(winsize.width/2-100, winsize.height/2-210)).easing(cc.easeElasticOut()));
		this.addChild(this.backBtn, 6);

		//show coins nums
		this.labelCoins = new cc.LabelTTF(this.totalCoin, "Helvetica", 50);
		this.labelCoins.setColor(cc.color(255, 255, 255));//white color
		this.labelCoins.setPosition(cc.p(winsize.width+100, winsize.height/2+128));
		this.labelCoins.setScale(0.3);
		this.addChild(this.labelCoins, 10);
		//this.labelCoins.retain();
		this.labelCoins.runAction(cc.moveTo(1, cc.p(winsize.width/2+50, winsize.height/2+128)).easing(cc.easeElasticOut()));

		this.buyMagnetBtn = new cc.Menu(new cc.MenuItemSprite(
				new cc.Sprite(res.ui.buy30),
				new cc.Sprite(res.ui.buy30),
				function(){
					//buy magnet
					if(this.totalCoin - 30 < 0){
						return;
					}
					this.totalCoin -= 30;
					this.magnetNum++;
					sys.localStorage.setItem("TotalCoin", this.totalCoin);
					sys.localStorage.setItem("magnet", this.magnetNum);
//					cc.audioEngine.playEffect(res.sound.button);
				}, this));
		this.buyMagnetBtn.setPosition(cc.p(winsize.width+80, winsize.height/2+70));
		this.buyMagnetBtn.attr({
			anchorX: 0,
			anchorY: 0
		});
		this.buyMagnetBtn.setScale(0.6);
		this.buyMagnetBtn.runAction(cc.moveTo(1, cc.p(winsize.width/2+80, winsize.height/2+70)).easing(cc.easeElasticOut()));
		this.addChild(this.buyMagnetBtn, 6);

		this.buyShoesBtn = new cc.Menu(new cc.MenuItemSprite(
				new cc.Sprite(res.ui.buy50),
				new cc.Sprite(res.ui.buy50),
				function(){
					//buy shoes
					if(this.totalCoin - 50 < 0){
						return;
					}
					this.totalCoin -= 50;
					this.shoesNum++;
					sys.localStorage.setItem("TotalCoin", this.totalCoin);
					sys.localStorage.setItem("shoes", this.shoesNum);
//					cc.audioEngine.playEffect(res.sound.button);
				}, this));
		this.buyShoesBtn.setPosition(cc.p(winsize.width+80, winsize.height/2-10));
		this.buyShoesBtn.attr({
			anchorX: 0,
			anchorY: 0
		});
		this.buyShoesBtn.setScale(0.6);
		this.buyShoesBtn.runAction(cc.moveTo(1, cc.p(winsize.width/2+80, winsize.height/2-10)).easing(cc.easeElasticOut()));
		this.addChild(this.buyShoesBtn, 6);

		this.buyRedshoesBtn = new cc.Menu(new cc.MenuItemSprite(
				new cc.Sprite(res.ui.buy50),
				new cc.Sprite(res.ui.buy50),
				function(){
					//buy red shoes
					if(this.totalCoin - 50 < 0){
						return;
					}
					this.totalCoin -= 50;
					this.redshoesNum++;
					sys.localStorage.setItem("TotalCoin", this.totalCoin);
					sys.localStorage.setItem("redshoes", this.redshoesNum);
//					cc.audioEngine.playEffect(res.sound.button);
				}, this));
		this.buyRedshoesBtn.setPosition(cc.p(winsize.width+80, winsize.height/2-90));
		this.buyRedshoesBtn.attr({
			anchorX: 0,
			anchorY: 0
		});
		this.buyRedshoesBtn.setScale(0.6);
		this.buyRedshoesBtn.runAction(cc.moveTo(1, cc.p(winsize.width/2+80, winsize.height/2-90)).easing(cc.easeElasticOut()));
		this.addChild(this.buyRedshoesBtn, 6);

		/**
		 * show prop nums of own
		 */
		//show magnet
		this.labelMagnet = new cc.LabelTTF(this.magnetNum, "Helvetica", 60);
		this.labelMagnet.setColor(cc.color(255, 255, 255));//white color
		this.labelMagnet.setPosition(cc.p(winsize.width+100, winsize.height/6-5));
		this.labelMagnet.setScale(0.3);
		this.addChild(this.labelMagnet, 10);
		this.labelMagnet.runAction(cc.moveTo(1, cc.p(winsize.width/2-70, winsize.height/6-5)).easing(cc.easeElasticOut()));

		//show shoes
		this.labelShoes = new cc.LabelTTF(this.magnetNum, "Helvetica", 60);
		this.labelShoes.setColor(cc.color(255, 255, 255));//white color
		this.labelShoes.setPosition(cc.p(winsize.width+100, winsize.height/6-5));
		this.labelShoes.setScale(0.3);
		this.addChild(this.labelShoes, 10);
		this.labelShoes.runAction(cc.moveTo(1, cc.p(winsize.width/2+20, winsize.height/6-5)).easing(cc.easeElasticOut()));

		//show redshoes
		this.labelRedshoes = new cc.LabelTTF(this.magnetNum, "Helvetica", 60);
		this.labelRedshoes.setColor(cc.color(255, 255, 255));//white color
		this.labelRedshoes.setPosition(cc.p(winsize.width+100, winsize.height/6-5));
		this.labelRedshoes.setScale(0.3);
		this.addChild(this.labelRedshoes, 10);
		this.labelRedshoes.runAction(cc.moveTo(1, cc.p(winsize.width/2+100, winsize.height/6-5)).easing(cc.easeElasticOut()));
	},
	
	onUpload: function () {
		var statistics = this.statistics;
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				cc.log(xhr.readyState);
			}
		};
		xhr.open('POST', /*'/**http://endless-journey-server.coding.io/game/scores*/'', true);
		xhr.setRequestHeader('Content-Type', 'application/json');
		var username = sys.localStorage.getItem("username");
		xhr.send(JSON.stringify({
			id: username,
			meter: statistics.meter,
			score: statistics.score,
			coins: statistics.coins
		}));
	},


	onEnter: function() {
		this._super();
		this.scheduleUpdate();
	},
	
	onExit: function() {
		this._super();
		this.unscheduleUpdate();
		//todo clear
	}

});