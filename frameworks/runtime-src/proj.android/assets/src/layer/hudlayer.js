var HudLayer = cc.Layer.extend({

    labelCoin: null,
    labelrun: null,
    pLayer: null,
    statistics: null,
    indicators: [],

    ctor: function (player, statistics, settings, camera) {
        this._super();
        this.player = player;
        this.statistics = statistics;
        this.camera = camera;
        /////////////////////////////////////////
        //Initialize & load hudlayer!
        ////////////////////////////////////////
        var size = cc.winSize;
        //todo try loading hudlayer_json with ccs.loadwithvisiblesize() instead!!
        //@Param:cc.Node()  : "node"
        //@Param:cc.Action() : "action"
        var ui = ccs.load(res.IAP.hudlayer_json);
                     var action = ui.action;
                            if(action){

                                ui.node.runAction(action);

                                action.gotoFrameAndPlay(0, true);
                            }
        this.addChild(ui.node);
        //|||||||||||||||||||||||||||||||||||||||||
        //Derive Panel_1 & Panel_2 & Panel_3
        //@Param:Panel_1:421
        //@Param:Panel_2:429
        //@Param:Panel_3:431
        var Panel_1 = ui.node.getChildByTag(421);
        cc.log("Panel_1 >>" + Panel_1);
        cc.log(Panel_1);
        var Panel_2 = ui.node.getChildByTag(429);
        cc.log("Panel_3 >>" + Panel_2);
        var Panel_3 = ui.node.getChildByTag(431);
        cc.log("Panel_3 >>" + Panel_3);
        //------------------------------------------
        //Panel_1 Objects Defined
        //AccessPoint:Panel_1.getChildByTag();
        //@Param:labelCoin:424
        this.labelCoin = Panel_1.getChildByTag(424);
        this.labelCoin.setString(statistics.score);
        cc.log(this.labelCoin);
        //|||||||||||||||||||||||||||||||||||||||||
        //Panel_2 Objects Defined
        //AccessPoint:Panel_2.getChildByTag();
        //@Param:LoadingBar_1:92
        //@Param:this.progress = this.energybar
        this.energybar = Panel_2.getChildByTag(92);
        cc.log("EnergyBar" + this.energybar);
        //||||||||||||||||||||||||||||||||||||||||
        //Panel_3 Objects Defined
        //AccessPoint:Panel_1.getChildByTag();
        //@Param:labelrun:432
        this.labelrun = Panel_3.getChildByTag(432);
        this.labelrun.setString("0 M");
        cc.log("label run >>"+this.labelrun);
        //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

        ///////////////////////////////////////////
        //**todo enable sprite coin animation!**//
        //////////////////////////////////////////
        //set the coin icon to very low opacity
        //if (player has picked up coins)
        //set opacity to 100% with particle plist
        //after 5 secs with no player.coins picked up
        //then slowly fade the icon opacity back to 20%
        //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


        ///////////////////////////////////////////
        //todo sound control btn
        //alto does this within pause layer or game over layer
        /////////////////////////////////////////
        // sound control btn
        //var soundOn = new cc.MenuItemImage(res.ui.soundOn);
        //var soundOff = new cc.MenuItemImage(res.ui.soundOff);
        //var toggler = new cc.MenuItemToggle(soundOn, soundOff,
        //    function () {
        //        if (settings.audioEnabled) {
        //            settings.audioEnabled = false;
        //        } else {
        //            settings.audioEnabled = true;
        //        }
        //    }, this);
        //
        //var soundBtn = new cc.Menu(toggler);
        //
        //soundBtn.setPosition(cc.p(winSize.width - 50, winSize.height - 45));
        //this.addChild(soundBtn);
        //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

   ///////////////////////////////////////////
    //// *****  prop set ******d
    ///////////////////////////////////
    //Derive Panel_Prop: 433 to access Prop Objects
    //@Param:magnetProp:435
    var Panel_Prop =  ui.node.getChildByTag(433);
    cc.log("Panel_Prop >>" + Panel_Prop);
    //||||||||||||||||||||||||||||||||||||
    //Define the Prop Objects
    //@Param:magnetProp:435
    //@Param:shoesProp:436
    //@Param:redshoesProp:437
    var magnetProp = Panel_Prop.getChildByTag(435);
        cc.log("flask" + magnetProp);
    var shoesProp = Panel_Prop.getChildByTag(436);
        cc.log("Egg" + shoesProp);
    var redshoesProp = Panel_Prop.getChildByTag(437);
        cc.log("Octopus" + redshoesProp);
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    /////////////////////////////////////////
    //Prop Event Listeners
    ////////////////////////////////////////
    magnetProp.addTouchEventListener(this.btn_magnetProp,this);
    shoesProp.addTouchEventListener(this.btn_shoesProp,this);
    redshoesProp.addTouchEventListener(this.btn_redshoesProp,this);
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

        //////////////////////////////////////////////////////////
        // ****** nums of prop *******
        ////////////////////////////////////////////////////////
 		//this.mNum = "2";
 		this.mNum = sys.localStorage.getItem("magnet");
		this.sNum = sys.localStorage.getItem("shoes");
		this.rNum = sys.localStorage.getItem("redshoes");
        //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//////////////////////////////////////////////////
//Derive the Label System Panel_Prop->Labels
//@Param this.MagnetNum :438
//@Param this.ShoesNum :440
//@Param this.RedshoesNum :439
this.MagentNum = Panel_Prop.getChildByTag(438);
this.MagentNum.setString(this.mNum);
//||||||||||||||||||||||||||||||||||||||||||||
this.ShoesNum = Panel_Prop.getChildByTag(440);
this.ShoesNum.setString(this.sNum);
//|||||||||||||||||||||||||||||||||||||||||||||
this.RedshoesNum = Panel_Prop.getChildByTag(439);
this.RedshoesNum.setString(this.rNum);
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        this.scheduleUpdate();
    },

    update: function (dt) {
        var statistics = this.statistics;
        var indicators = this.indicators;
        var hero = this.player;

        // update the coins and meters.
        this.labelCoin.setString(statistics.coins);
        this.labelrun.setString(statistics.meter + " m");

        // update the indicators.
        for (var i = 0; i < indicators.length; i++) {
            if (ind.update) {
                ind.update(dt, hero);
            }
        }
    },

    addIndicator: function (ind) {
        this.addChild(ind.sprite);
        var indicators = this.indicators;
        var statistics = this.statistics;
        var camera = this.camera;
        var winSize = cc.director.getWinSize();

        var px = camera.x + winSize.width / 2;
        var py = indicators.length * 100;
        var action = (new cc.MoveTo(0.5, cc.p(px + 200, py + 300))).easing(cc.easeBackIn());
        ind.sprite.runAction(action);
        indicators.push(ind);
    },
    removeIndicator: function (ind) {
        var indicators = this.indicators;
        for (var i = 0; i < indicators.length; i++) {
            if (indicators[i] == ind) {
                indicators.splice(i, 1).sprite.removeFromParent();
                break;
            }
        }
    },
    consumeMagnet: function () {
        this.progress.setScaleX(0.01);
        var actionTo1 = cc.scaleTo(1, 1.03, 1);
        var actionTo = cc.scaleTo(7.5, 0.5, 0.7);
        var actionTo2 = cc.scaleTo(7.5, 0.0001, 1);
        var seq = cc.sequence(actionTo1, actionTo, actionTo2);
        this.progress.runAction(seq);
    },
     btn_magnetProp: function(sender, type)
                    {
                        switch (type)
                        {
                        case ccui.Widget.TOUCH_BEGAN:
                         cc.log("btn_magnetProp");
                            if(this.mNum > 0) {
                                this.magnetEffect = new MagnetEffect();
                                this.addChild(this.magnetEffect, 2);
                                this.magnetEffect.getMagnet();

                                this.player.getMagnet();
                                this.consumeMagnet();
                                setTimeout(function(){
                                    this.player.loseMagnet();
                                    this.magnetEffect.loseMagnet();
                                }.bind(this), 15000);

//                                cc.audioEngine.playEffect(res.sound.magnet);

                                this.mNum--;
                                sys.localStorage.setItem("magnet", this.mNum);
                                this.MagentNum.setString(this.mNum);
                            }
                            break;
                        case ccui.Widget.TOUCH_ENDED:
                            break;
                        case ccui.Widget.TOUCH_CANCELLED:
                            break;
                        }
                    },

     btn_shoesProp: function(sender, type)
                    {
                        switch (type)
                        {
                        case ccui.Widget.TOUCH_BEGAN:

                        cc.log("btn_shoesProp");
                            if(this.sNum > 0) {
                                this.player.getShoesAndSpeedUp();
                                this.sNum--;
                                sys.localStorage.setItem("shoes", this.sNum);
                                this.ShoesNum.setString(this.sNum);

                                this.magnetEffect = new MagnetEffect();
                                this.addChild(this.magnetEffect, 2);
                                this.magnetEffect.getMagnet();

                                setTimeout(function(){
                                    this.player.loseMagnet();
                                    this.magnetEffect.loseMagnet();
                                }.bind(this), 15000);

//                                cc.audioEngine.playEffect(res.sound.speedup);
                            }
                            break;
                        case ccui.Widget.TOUCH_ENDED:

                            cc.log("touch ended for shoes");
                            break;
                        case ccui.Widget.TOUCH_CANCELLED:
                            break;
                        }
                    },
     btn_redshoesProp: function(sender, type)
                    {
                        switch (type)
                        {
                        case ccui.Widget.TOUCH_BEGAN:
                        cc.log("btn_redshoesProp");
                        ////////////////////////////////////////////////////////
                            if(this.rNum > 0) {
                                this.player.getRedshoesAndSlowDown();
                                this.rNum--;
                                sys.localStorage.setItem("redshoes", this.rNum);
                                this.RedshoesNum.setString(this.rNum);
                                this.magnetEffect = new MagnetEffect();
                                this.addChild(this.magnetEffect, 2);
                                this.magnetEffect.getMagnet();

                                setTimeout(function(){
                                    this.player.loseMagnet();
                                    this.magnetEffect.loseMagnet();
                                }.bind(this), 15000);

//                                cc.audioEngine.playEffect(res.sound.magnet);
                            }
                            break;
                        ///////////////////////////////////////////////////////////
                        case ccui.Widget.TOUCH_ENDED:
                            cc.log("touch Ended for redshoes ");
                            break;
                        case ccui.Widget.TOUCH_CANCELLED:
                            break;
                        }
                    }

});