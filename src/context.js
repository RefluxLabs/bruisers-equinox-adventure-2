var context = (function () {
    var welcomeScene = null;
    var playScene = null;
    var pageviewScene = null;

    function Context() {
        this.welcomeScene = null;
        this.playScene = null;
        this.pageviewScene = null;
    }

    Context.prototype = {
        get welcomeScene() {
            var scene = this.welcomeScene || (welcomeScene = new WelcomeScene());
            return scene;
        },
        get pageviewScene() {
        var scene = this.pageviewScene || (pageviewScene = new PageViewScene());
            return scene;
        },
        get playScene() {
            var scene = this.playScene || (playScene = new PlayScene());
            return scene;
        }
    };

    return Context;
}());