///////////////////////////////////////////////////
/*
PageViewLayer({@Extends from PageViewScene() below
PageViewLayer is a perfect example of coding a cocosstudio scene node
that has a layer as a dependent node
This layer shows how to: 1. Load json
                         2. Debug json
                         3. Declare Global and Local Widgets
                         4. Define Global and Local Widgets TouchEventListeners
*/
////////////////////////////////////////////////////
var StoreLayer = cc.Layer.extend({
    sprite:null,
    ctor:function () {
        //////////////////////////////
        // 1. super init first
        this._super();
        /////////////////////////////
        // 2. add a menu item with "X" image, which is clicked to quit the program
        //    you may modify it.
        // ask the window size
        var size = cc.winSize;
        ////////////////////////////////////
        //Initialize and Load Json Scene
        var storescene = ccs.load(res.IAP.StoreScene_json);
        this.addChild(storescene.node);
        cc.log(storescene);
        this.openStore = true;
        cc.audioEngine.stopMusic();
        if(canMusicPlaying) {
//			cc.audioEngine.playMusic(res.sound.shopping, true);
		};

//        sdkbox.PluginGoogleAnalytics.init();

        /////////////////////////////////////////////////
        //Scene Debug Module logs data to ensure it works
        var StoreLayer = storescene.node.getChildByTag(64);
        cc.log(StoreLayer);
        //var data = scene.getUserData();
        //cc.log(data);
       
        ////////////////////////////////////////////
        //Define Global buttons within StoreLayer
        //@Param:btn_play:tag#()
        //@Param:btn_back:tag#(50)
        var btn_back = StoreLayer.getChildByTag(50);
         cc.log(btn_back);
        //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


             /////////////////////////////////////////////
             //Declare Scope of  to access buttons
             //@param:ListView_1:48
             var ListView_1 = StoreLayer.getChildByTag(48);
             cc.log("ListView_1");
             cc.log(ListView_1);
             //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

            var Item_1 = ListView_1.getChildByTag(47);
            cc.log(Item_1);
            var btn_buymagnet = Item_1.getChildByTag(108);


            var Item_2 = ListView_1.getChildByTag(111);
            cc.log(Item_2);
            var btn_buyshoes = Item_2.getChildByTag(112);

            var Item_3 = ListView_1.getChildByTag(115);
            cc.log(Item_3);
            var btn_buyredshoes  = Item_3.getChildByTag(116);
            cc.log("buy red shoes button! >> " + btn_buyredshoes);
            cc.log(btn_buyredshoes);

            ////////////////////////////////////////////////
            //todo Derive Labels
            ///////////////////////////////////////////////
            //Panel_Stats object should gives access to:
            //local variables...txt.coins ..etc
            var Panel_Stats = StoreLayer.getChildByTag(104);
            cc.log("Panel_Stats!");
            cc.log(Panel_Stats);
            //||||||||||||||||||||||||||||||||||||||||||||
                //Local Variables
                //@Param:txt_magnet:70
                //@Param:txt_shoes:73
                //@Param:txt_redshoes:74
                //@Param:txt_coins:141
                //||||||||||||||||||||||||||||||||||||||||||||||
                this.labelMagnet = Panel_Stats.getChildByTag(70);
                cc.log("labelMagnet below!");
                cc.log(this.labelMagnet);

                this.labelShoes = Panel_Stats.getChildByTag(73);
                cc.log("labelShoes below!");
                cc.log(this.labelShoes);

                this.labelRedShoes = Panel_Stats.getChildByTag(74);
                cc.log("labelRedShoes below!");
                cc.log(this.labelRedShoes);

                this.labelCoins = Panel_Stats.getChildByTag(111);
                cc.log("labelCoins below!");
                cc.log(this.labelCoins);
                this.labelCoins.setString("94");//this.totalCoin
                cc.log(this.labelCoins);
                cc.log(this.totalCoin);
            //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

       /////////////////////////////////////////////////////
       //Declare Local TouchEventListeners for above params
       //@Local Param:btn_buymagnet
       //@Local Param:btn_buyshoes
       //@Local Param:btn_buyredshoes
        btn_buymagnet.addTouchEventListener(this.btn_buymagnetEvent, this);
        btn_buyredshoes.addTouchEventListener(this.btn_buyredshoesEvent, this);
        btn_buyshoes.addTouchEventListener(this.btn_buyshoesEvent, this);


        ///////////////////////////////////////////////////////////////////////
         //Load Global Variables to write to system <-NEEDS to happen before
         //declaration of btn_buymagnet ..... & others!
         //Defined within resource.js
         //@Variable: totalCoin : "TotalCoin"
         //@Variable: magnetNum : "magnet"
         //@Variable: shoesNum : "shoes"
         //@Variable: redshoesNum : " redshoes"
          this.totalCoin = sys.localStorage.getItem("TotalCoin");
          this.magnetNum = sys.localStorage.getItem("magnet");
          this.shoesNum = sys.localStorage.getItem("shoes");
          this.redshoesNum = sys.localStorage.getItem("redshoes");
       //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\



       //////////////////////////////////////////////////
       //Declare TouchEventListeners for above params
       //@Global Param:btn_back
        btn_back.addTouchEventListener(this.btn_backEvent, this);
       //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


    this.scheduleUpdate();
},
    update: function(dt){
        if (this.openStore) {
            this.labelCoins.setString(this.totalCoin);
            this.labelMagnet.setString(this.magnetNum);
            this.labelShoes.setString(this.shoesNum);
            this.labelRedShoes.setString(this.redshoesNum);
        }
    },
    //this is called when the user interacts with the button
       btn_backEvent: function(sender, type)
        {
            switch (type)
            {
            case ccui.Widget.TOUCH_BEGAN:
                break;
            case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:
                cc.log("btn_back touch ended!")
                cc.director.popScene();
                break;
            case ccui.Widget.TOUCH_CANCELLED:
                break;
            }
        }
        ,
    //this is called when the user interacts with the button
       btn_buymagnetEvent: function(sender, type)
        {
            switch (type)
            {
            case ccui.Widget.TOUCH_BEGAN:
                break;
           case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:
                cc.log("buy magnet  button touch ended!")
                               // this.totalCoin +=70;
                    //buy magnet
                    if(this.totalCoin - 30 < 0){
                            return;
                            }
                    this.totalCoin -= 30;
                    this.magnetNum++;
                    sys.localStorage.setItem("TotalCoin", this.totalCoin);
                    sys.localStorage.setItem("magnet", this.magnetNum);
//                    cc.audioEngine.playEffect(res.sound.button);

                break;
            case ccui.Widget.TOUCH_CANCELLED:
                break;
            }
        },
        //this is called when the user interacts with the button
               btn_buyshoesEvent: function(sender, type)
                {
                    switch (type)
                    {
                    case ccui.Widget.TOUCH_BEGAN:
                        break;
                   case ccui.Widget.TOUCH_MOVED:
                        break;
                    case ccui.Widget.TOUCH_ENDED:
                        cc.log("Purple shoes button touch ended!")
                            //buy shoes
                            if(this.totalCoin - 50 < 0){
                                return;
                            }
                            this.totalCoin -= 50;
                            this.shoesNum++;
                            sys.localStorage.setItem("TotalCoin", this.totalCoin);
                            sys.localStorage.setItem("shoes", this.shoesNum);
//                            cc.audioEngine.playEffect(res.sound.button);
                        break;
                    case ccui.Widget.TOUCH_CANCELLED:
                        break;
                    }
                },
                //this is called when the user interacts with the button
                       btn_buyredshoesEvent: function(sender, type)
                        {
                            switch (type)
                            {
                            case ccui.Widget.TOUCH_BEGAN:
                                break;
                           case ccui.Widget.TOUCH_MOVED:
                                break;
                            case ccui.Widget.TOUCH_ENDED:
                                cc.log("Red Shoes button touch ended!")
                            //buy red shoes
                            if(this.totalCoin - 50 < 0){
                                return;
                            }
                            this.totalCoin -= 50;
                            this.redshoesNum++;
                            sys.localStorage.setItem("TotalCoin", this.totalCoin);
                            sys.localStorage.setItem("redshoes", this.redshoesNum);
//                      cc.audioEngine.playEffect(res.sound.button);
                   this.createTestMenu();

                                break;
                            case ccui.Widget.TOUCH_CANCELLED:
                                break;
                            }
                        },

                    ///////////////////////////////////////
                    //** sdkbox_ga plugin debugger!  ** //
                    createTestMenu:function() {
                    cc.MenuItemFont.setFontName("sans");
                    var size = cc.director.getWinSize();

                    sdkbox.PluginGoogleAnalytics.init();
                    var menu = new cc.Menu(
                    new cc.MenuItemFont("log event", function() {
                    sdkbox.PluginGoogleAnalytics.logEvent("Test", "Click", "", 1);
                    sdkbox.PluginGoogleAnalytics.dispatchHits();
                    cc.log("sdkbox.PluginGoogleAnalytics.logEvent(\"Test\", \"Click\", \"\", 1);");
                    }),

                    new cc.MenuItemFont("log exception", function() {
                    sdkbox.PluginGoogleAnalytics.logException("Test Exception", true);
                    sdkbox.PluginGoogleAnalytics.dispatchHits();
                    cc.log("sdkbox.PluginGoogleAnalytics.logException(\"Test Exception\", true);");
                    }),

                    new cc.MenuItemFont("log social", function() {
                    sdkbox.PluginGoogleAnalytics.logSocial("facebook", "share", "sdkbox");
                    sdkbox.PluginGoogleAnalytics.dispatchHits();
                    cc.log("sdkbox.PluginGoogleAnalytics.logSocial(\"facebook\", \"share\", \"sdkbox\");");
                    }));

                    menu.alignItemsVerticallyWithPadding(5);
                    menu.x = size.width/2;
                    menu.y = size.height/2;
                    this.addChild(menu);
                                       }

});

var StoreScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new StoreLayer();
        this.addChild(layer);
    }
});

